const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return false;
		} else {
			return true; 
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
		// 10 = salt  ?
	})

	return newUser.save().then((user,error) => {
		if(error){
			return `User registration failed!`
		} 
		return `User ${reqBody.firstName} has successfuly been created!`
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				// Generate an Access
				return {access: auth.createAccessToken(result)}
			}
		}
	})
}

module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.userId).then(result =>{
		return result; 
	})
}